# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, GuardStateDecorator
from guardian.manager import NodeManager
from guardian.ligopath import userapps_path
import cdsutils
import time
import math
import subprocess
from filterconfig import filter_list4

CHANNELS_PER_LOOP = 2001

nominal = 'RUN_TEST'
request = 'RUN_TEST'


class INIT(GuardState):
    index = 0
    request = True

    def run(self):
        return True

class IDLE(GuardState):
    index = 10
    goto = True

    def run(self):
        return True

class RUN_TEST(GuardState):
    index = 20
    goto = True

    def main(self):
       self.x = 0

    def run(self):
        for count in range(self.x*CHANNELS_PER_LOOP,(self.x+1)*CHANNELS_PER_LOOP):
            if count >= len(filter_list4):
                self.x = -1
                time.sleep(1)
                break
            value = ezca[filter_list4[count]]
            if value == '-inf':
                notify(filter_list4[count] + ' is -inf')
            elif float(value) == float(1e20):
                notify(filter_list4[count] + ' is 1e20')
        self.x = self.x + 1
        return True

edges = [
    ('INIT','IDLE'),
    ('IDLE','RUN_TEST'),
    ('RUN_TEST','IDLE'),
    ]
